import React, {Component} from 'react';
import ReactDOM from 'react-dom';
// import rd3 from 'rd3'
// import PropTypes from 'prop-types'; // ES6
// import rd3 from 'react-d3';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import './App.scss';
 
// const ReactD3 = require('react-d3');
import _ from 'lodash'
import moment from 'moment'

class App extends Component {
  constructor(props){
    super(props)
    this.data = [];
    this.handleBTCClick = this.handleBTCClick.bind(this, arguments);
    this.handleETHClick = this.handleETHClick.bind(this, arguments);
  }

  btnTapped() {
    console.log('tapped');
  }
  
  handleBTCClick(e, route){ console.log('activate: '+route)
    //API Daily BTC
    fetch('http://'+window.location.hostname+':7000'+route, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response)=>response.json()).then((result)=>{

     ReactDOM.render(
       <LineChart width={1400} height={500} data={result} margin={{top: 5, right: 30, left: 20, bottom: 5}}>
           <XAxis dataKey="name"/>
           <YAxis dataKey="price" />
           <CartesianGrid strokeDasharray="3 3"/>
           
           <Legend />
           <Line type="monotone" dataKey="price" stroke="#8884d8" activeDot={{r: 8}}/>
          </LineChart>
          ,
        document.getElementById('btc')
     );  
    });
  }

  handleETHClick(e, route){ console.log('activate: '+route)
    //API Daily BTC
    fetch('http://'+window.location.hostname+':7000'+route, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response)=>response.json()).then((result)=>{

     ReactDOM.render(
       <LineChart width={1400} height={500} data={result} margin={{top: 5, right: 30, left: 20, bottom: 5}}>
           <XAxis dataKey="name"/>
           <YAxis dataKey="price" />
           <CartesianGrid strokeDasharray="3 3"/>
           
           <Legend />
           <Line type="monotone" dataKey="price" stroke="#8884d8" activeDot={{r: 8}}/>
          </LineChart>
          ,
        document.getElementById('eth')
     );  
    });
  }

  render() {
    return <div>
    <h1>Bitcoins (CLP)</h1>
    <div id="btc"></div>
    <button type="button" onClick={ () => this.handleBTCClick('/btc_daily.json')}>Diario</button>
    <button type="button" onClick={ () => this.handleBTCClick('/btc_monthly.json')}>Mensual</button>
    <h1>Ethereum (CLP)</h1>
    <div id="eth"></div>
    <button type="button" onClick={ () => this.handleETHClick('/eth_daily.json')}>Diario</button>
    <button type="button" onClick={ () => this.handleETHClick('/eth_monthly.json')}>Mensual</button>
    </div>
  }
}



ReactDOM.render(
<App />, 
document.getElementById('root')
);

if (module.hot) {
  module.hot.accept();
}