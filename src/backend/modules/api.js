import https from 'https'
import _ from 'lodash'
import redis from 'redis'

const BTC_DIGITAL_CURRENCY_INTRADAY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=BTC&market=CLP&apikey=1B94810TS846HL0M'
const BTC_DIGITAL_CURRENCY_MONTHLY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=BTC&market=CLP&apikey=1B94810TS846HL0M'
const ETH_DIGITAL_CURRENCY_INTRADAY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=ETH&market=CLP&apikey=1B94810TS846HL0M'
const ETH_DIGITAL_CURRENCY_MONTHLY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=ETH&market=CLP&apikey=1B94810TS846HL0M'

const Api = {};
const RedisClient = redis.createClient();

Api.getDailyBtc = () => {
	return new Promise ((resolve, reject) => {
		//simulated error
		if (Math.random(0, 1) < 0.1){
			throw new Error('How unfortunate! The API Request Failed'); 
			reject('False api error');
		} 
		
		//Cache
		RedisClient.get('daily_btc', (err, data) => {
	    if(err || data === null) {
			https.get(BTC_DIGITAL_CURRENCY_INTRADAY_URL, (resp) => {
					let data = '';
					resp.on('data', (chunk) => {
				  	data += chunk;
				  });

			  	resp.on('end', () => {
			  	var result = JSON.parse(data)
			  	let by_hours = [];

			  	_.forEach(result["Time Series (Digital Currency Intraday)"], (value, key)=>{
			  		if(_.endsWith(key, "00:00")){
			  			// _.assign(by_hours,{ time: key, value: value })
			  			by_hours.push({ time: key, value: value })
			  		}
			  	});

			  	var time_serie = _.reverse(_.slice(by_hours, -24)) ;

			  	//Cache storing
			  	RedisClient.set('daily_btc', JSON.stringify(time_serie), 'EX', 60 * 60, (err) => {
			  		if(err){
							throw new Error('Cache failed!')
			  		}
			  		else{
							resolve(time_serie);
			  		} 
			  	})
			  });
			}).on("error", (err) => {
				console.log("Error: " + err.message);
			});        
    } else {
        resolve(data);
				console.log('en cache');	
    }
		});	
	})	
};

Api.getMonthlyBtc = () => {
	return new Promise ((resolve, reject) => {
		//simulated error
		if (Math.random(0, 1) < 0.1){
			throw new Error('How unfortunate! The API Request Failed'); 
			reject('False api error');
		} 
		
		//Cache
		RedisClient.get('monthly_btc', (err, data) => {
	    if(err || data === null) {
			https.get(BTC_DIGITAL_CURRENCY_MONTHLY_URL, (resp) => {
					let data = '';
					resp.on('data', (chunk) => {
				  	data += chunk;
				  });

			  	resp.on('end', () => {
			  	var result = JSON.parse(data)
			  	let by_months = [];

			  	_.forEach(result["Time Series (Digital Currency Monthly)"], (value, key)=>{
		  			by_months.push({ time: key, value: value })
			  	});

			  	let time_serie = _.reverse(_.slice(by_months, -12)) ; //last 12 months

			  	//Cache storing
			  	RedisClient.set('monthly_btc', JSON.stringify(time_serie),'EX', 60 * 60 * 24,(err) => {
			  		if(err){
							throw new Error('Cache failed!')
			  		}
			  		else{
							resolve(time_serie);
			  		} 
			  	})
			  });
			}).on("error", (err) => {
				console.log("Error: " + err.message);
			});        
    } else {
        resolve(data);
				console.log('en cache');	
    }
		});	
	})	
};

Api.getDailyEth = () => {
	return new Promise ((resolve, reject) => {
		//simulated error
		if (Math.random(0, 1) < 0.1){
			throw new Error('How unfortunate! The API Request Failed'); 
			reject('False api error');
		} 
		
		//Cache
		RedisClient.get('daily_eth', (err, data) => {
	    if(err || data === null) {
			https.get(ETH_DIGITAL_CURRENCY_INTRADAY_URL, (resp) => {
					let data = '';
					resp.on('data', (chunk) => {
				  	data += chunk;
				  });

			  	resp.on('end', () => {
			  	var result = JSON.parse(data)
			  	let by_hours = [];

			  	_.forEach(result["Time Series (Digital Currency Intraday)"], (value, key)=>{
			  		if(_.endsWith(key, "00:00")){
			  			// _.assign(by_hours,{ time: key, value: value })
			  			by_hours.push({ time: key, value: value })
			  		}
			  	});

			  	var time_serie = _.reverse(_.slice(by_hours, -24)) ;

			  	//Cache storing
			  	RedisClient.set('daily_eth', JSON.stringify(time_serie),'EX', 60 * 60,(err) => {
			  		if(err){
							throw new Error('Cache failed!')
			  		}
			  		else{
							resolve(time_serie);
			  		} 
			  	})
			  });
			}).on("error", (err) => {
				console.log("Error: " + err.message);
			});        
    } else {
        resolve(data);
				console.log('en cache');	
    }
		});	
	})	
};

Api.getMonthlyEth = () => {
	return new Promise ((resolve, reject) => {
		//simulated error
		if (Math.random(0, 1) < 0.1){
			throw new Error('How unfortunate! The API Request Failed'); 
			reject('False api error');
		} 
		
		//Cache
		RedisClient.get('monthly_eth', (err, data) => {
	    if(err || data === null) {
			https.get(ETH_DIGITAL_CURRENCY_MONTHLY_URL, (resp) => {
					let data = '';
					resp.on('data', (chunk) => {
				  	data += chunk;
				  });

			  	resp.on('end', () => {
			  	var result = JSON.parse(data)
			  	let by_months = [];

			  	_.forEach(result["Time Series (Digital Currency Monthly)"], (value, key)=>{
		  			by_months.push({ time: key, value: value })
			  	});

			  	let time_serie = _.reverse(_.slice(by_months, -12)) ; //last 12 months

			  	//Cache storing
			  	RedisClient.set('monthly_eth', JSON.stringify(time_serie), 'EX', 60 * 60 * 24, (err) => {
			  		if(err){
							throw new Error('Cache failed!')
			  		}
			  		else{
							resolve(time_serie);
			  		} 
			  	})
			  });
			}).on("error", (err) => {
				console.log("Error: " + err.message);
			});        
    } else {
        resolve(data);
				console.log('en cache');	
    }
		});	
	})	
};

export default Api