import http from 'http'
import url from 'url'
import Api from './api.js' 
import _ from 'lodash'
import promiseRetry from 'promise-retry'
import moment from 'moment'

http.createServer(function(request, response){
	var headers = {};

    // set header to handle the CORS
    headers['Access-Control-Allow-Origin'] = '*';
    headers['Access-Control-Allow-Headers'] = 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With';
    headers['Access-Contrl-Allow-Methods'] = 'PUT, POST, GET, DELETE, OPTIONS';
    headers["Access-Control-Max-Age"] = '86400';
    headers["Content-Type"] = 'application/json';
    response.writeHead(200, headers);

	console.log(url.parse(request.url).pathname)

	switch(url.parse(request.url).pathname){
		case '/btc_daily.json':
			promiseRetry(function (retry, number) {
			    console.log('intento numero', number);

			    return Api.getDailyBtc()
			    .catch(retry);
			})
			.then(function (result) {
			    if(_.isString(result)) result = JSON.parse(result)

				let clp_data = [];
				// let usd_data = [];

				_.forEach(result, (value, key) => {
					clp_data.push({name: moment(value.time).format('h:mm').toString(), price: _.parseInt(value.value["1a. price (CLP)"]) })
					// usd_data.push({name: moment(value.time).format('h:mm').toString(), price: _.parseInt(value.value["1b. price (USD)"]) })
				});

				if(_.isObject(clp_data)) clp_data = JSON.stringify(clp_data)

				response.write(clp_data);
				response.end();	
			}, function (err) {
			    //..
			});

		break;

		case '/btc_monthly.json':
			promiseRetry(function (retry, number) {
			    console.log('intento numero', number);

			    return Api.getMonthlyBtc()
			    .catch(retry);
			})
			.then(function (result) {
				if(_.isString(result)) result = JSON.parse(result)

				let months = [];
				// let usd_data = [];

				_.forEach(result, (value, key) => {
					months.push({name: moment(value.time).format('MMMM').toString(), price: _.parseInt(value.value["4a. close (CLP)"]) })
				});

			    if(_.isObject(months)) months = JSON.stringify(months)
				response.write(months);
				response.end();	
			}, function (err) {
			    //..
			});
		break;

		case '/eth_daily.json':
			promiseRetry(function (retry, number) {
			    console.log('intento numero', number);

			    return Api.getDailyEth()
			    .catch(retry);
			})
			.then(function (result) {
			    if(_.isString(result)) result = JSON.parse(result)

				let clp_data = [];
				// let usd_data = [];

				_.forEach(result, (value, key) => {
					clp_data.push({name: moment(value.time).format('h:mm').toString(), price: _.parseInt(value.value["1a. price (CLP)"]) })
					// usd_data.push({name: moment(value.time).format('h:mm').toString(), price: _.parseInt(value.value["1b. price (USD)"]) })
				});

				if(_.isObject(clp_data)) clp_data = JSON.stringify(clp_data)

				response.write(clp_data);
				response.end();	
			}, function (err) {
			    //..
			});			
		break;

		case '/eth_monthly.json':
			promiseRetry(function (retry, number) {
			    console.log('intento numero', number);

			    return Api.getMonthlyEth()
			    .catch(retry);
			})
			.then(function (result) {
				if(_.isString(result)) result = JSON.parse(result)

				let months = [];
				// let usd_data = [];

				_.forEach(result, (value, key) => {
					months.push({name: moment(value.time).format('MMMM').toString(), price: _.parseInt(value.value["4a. close (CLP)"]) })
				});

			    if(_.isObject(months)) months = JSON.stringify(months)
				response.write(months);
				response.end();	
			}, function (err) {
			    //..
			});	
		break;
		default:
			response.write('No valid path');
			response.end();
	}


}).listen(7000);