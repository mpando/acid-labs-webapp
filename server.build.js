/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _http = __webpack_require__(2);

var _http2 = _interopRequireDefault(_http);

var _url = __webpack_require__(3);

var _url2 = _interopRequireDefault(_url);

var _api = __webpack_require__(4);

var _api2 = _interopRequireDefault(_api);

var _lodash = __webpack_require__(0);

var _lodash2 = _interopRequireDefault(_lodash);

var _promiseRetry = __webpack_require__(7);

var _promiseRetry2 = _interopRequireDefault(_promiseRetry);

var _moment = __webpack_require__(8);

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_http2.default.createServer(function (request, response) {
	var headers = {};

	// set header to handle the CORS
	headers['Access-Control-Allow-Origin'] = '*';
	headers['Access-Control-Allow-Headers'] = 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With';
	headers['Access-Contrl-Allow-Methods'] = 'PUT, POST, GET, DELETE, OPTIONS';
	headers["Access-Control-Max-Age"] = '86400';
	headers["Content-Type"] = 'application/json';
	response.writeHead(200, headers);

	console.log(_url2.default.parse(request.url).pathname);

	switch (_url2.default.parse(request.url).pathname) {
		case '/btc_daily.json':
			(0, _promiseRetry2.default)(function (retry, number) {
				console.log('intento numero', number);

				return _api2.default.getDailyBtc().catch(retry);
			}).then(function (result) {
				if (_lodash2.default.isString(result)) result = JSON.parse(result);

				var clp_data = [];
				// let usd_data = [];

				_lodash2.default.forEach(result, function (value, key) {
					clp_data.push({ name: (0, _moment2.default)(value.time).format('h:mm').toString(), price: _lodash2.default.parseInt(value.value["1a. price (CLP)"]) });
					// usd_data.push({name: moment(value.time).format('h:mm').toString(), price: _.parseInt(value.value["1b. price (USD)"]) })
				});

				if (_lodash2.default.isObject(clp_data)) clp_data = JSON.stringify(clp_data);

				response.write(clp_data);
				response.end();
			}, function (err) {
				//..
			});

			break;

		case '/btc_monthly.json':
			(0, _promiseRetry2.default)(function (retry, number) {
				console.log('intento numero', number);

				return _api2.default.getMonthlyBtc().catch(retry);
			}).then(function (result) {
				if (_lodash2.default.isString(result)) result = JSON.parse(result);

				var months = [];
				// let usd_data = [];

				_lodash2.default.forEach(result, function (value, key) {
					months.push({ name: (0, _moment2.default)(value.time).format('MMMM').toString(), price: _lodash2.default.parseInt(value.value["4a. close (CLP)"]) });
				});

				if (_lodash2.default.isObject(months)) months = JSON.stringify(months);
				response.write(months);
				response.end();
			}, function (err) {
				//..
			});
			break;

		case '/eth_daily.json':
			(0, _promiseRetry2.default)(function (retry, number) {
				console.log('intento numero', number);

				return _api2.default.getDailyEth().catch(retry);
			}).then(function (result) {
				if (_lodash2.default.isString(result)) result = JSON.parse(result);

				var clp_data = [];
				// let usd_data = [];

				_lodash2.default.forEach(result, function (value, key) {
					clp_data.push({ name: (0, _moment2.default)(value.time).format('h:mm').toString(), price: _lodash2.default.parseInt(value.value["1a. price (CLP)"]) });
					// usd_data.push({name: moment(value.time).format('h:mm').toString(), price: _.parseInt(value.value["1b. price (USD)"]) })
				});

				if (_lodash2.default.isObject(clp_data)) clp_data = JSON.stringify(clp_data);

				response.write(clp_data);
				response.end();
			}, function (err) {
				//..
			});
			break;

		case '/eth_monthly.json':
			(0, _promiseRetry2.default)(function (retry, number) {
				console.log('intento numero', number);

				return _api2.default.getMonthlyEth().catch(retry);
			}).then(function (result) {
				if (_lodash2.default.isString(result)) result = JSON.parse(result);

				var months = [];
				// let usd_data = [];

				_lodash2.default.forEach(result, function (value, key) {
					months.push({ name: (0, _moment2.default)(value.time).format('MMMM').toString(), price: _lodash2.default.parseInt(value.value["4a. close (CLP)"]) });
				});

				if (_lodash2.default.isObject(months)) months = JSON.stringify(months);
				response.write(months);
				response.end();
			}, function (err) {
				//..
			});
			break;
		default:
			response.write('No valid path');
			response.end();
	}
}).listen(7000);

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _https = __webpack_require__(5);

var _https2 = _interopRequireDefault(_https);

var _lodash = __webpack_require__(0);

var _lodash2 = _interopRequireDefault(_lodash);

var _redis = __webpack_require__(6);

var _redis2 = _interopRequireDefault(_redis);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BTC_DIGITAL_CURRENCY_INTRADAY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=BTC&market=CLP&apikey=1B94810TS846HL0M';
var BTC_DIGITAL_CURRENCY_MONTHLY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=BTC&market=CLP&apikey=1B94810TS846HL0M';
var ETH_DIGITAL_CURRENCY_INTRADAY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=ETH&market=CLP&apikey=1B94810TS846HL0M';
var ETH_DIGITAL_CURRENCY_MONTHLY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=ETH&market=CLP&apikey=1B94810TS846HL0M';

var Api = {};
var RedisClient = _redis2.default.createClient();

Api.getDailyBtc = function () {
	return new Promise(function (resolve, reject) {
		//simulated error
		if (Math.random(0, 1) < 0.1) {
			throw new Error('How unfortunate! The API Request Failed');
			reject('False api error');
		}

		//Cache
		RedisClient.get('daily_btc', function (err, data) {
			if (err || data === null) {
				_https2.default.get(BTC_DIGITAL_CURRENCY_INTRADAY_URL, function (resp) {
					var data = '';
					resp.on('data', function (chunk) {
						data += chunk;
					});

					resp.on('end', function () {
						var result = JSON.parse(data);
						var by_hours = [];

						_lodash2.default.forEach(result["Time Series (Digital Currency Intraday)"], function (value, key) {
							if (_lodash2.default.endsWith(key, "00:00")) {
								// _.assign(by_hours,{ time: key, value: value })
								by_hours.push({ time: key, value: value });
							}
						});

						var time_serie = _lodash2.default.reverse(_lodash2.default.slice(by_hours, -24));

						//Cache storing
						RedisClient.set('daily_btc', JSON.stringify(time_serie), 'EX', 60 * 60, function (err) {
							if (err) {
								throw new Error('Cache failed!');
							} else {
								resolve(time_serie);
							}
						});
					});
				}).on("error", function (err) {
					console.log("Error: " + err.message);
				});
			} else {
				resolve(data);
				console.log('en cache');
			}
		});
	});
};

Api.getMonthlyBtc = function () {
	return new Promise(function (resolve, reject) {
		//simulated error
		if (Math.random(0, 1) < 0.1) {
			throw new Error('How unfortunate! The API Request Failed');
			reject('False api error');
		}

		//Cache
		RedisClient.get('monthly_btc', function (err, data) {
			if (err || data === null) {
				_https2.default.get(BTC_DIGITAL_CURRENCY_MONTHLY_URL, function (resp) {
					var data = '';
					resp.on('data', function (chunk) {
						data += chunk;
					});

					resp.on('end', function () {
						var result = JSON.parse(data);
						var by_months = [];

						_lodash2.default.forEach(result["Time Series (Digital Currency Monthly)"], function (value, key) {
							by_months.push({ time: key, value: value });
						});

						var time_serie = _lodash2.default.reverse(_lodash2.default.slice(by_months, -12)); //last 12 months

						//Cache storing
						RedisClient.set('monthly_btc', JSON.stringify(time_serie), 'EX', 60 * 60 * 24, function (err) {
							if (err) {
								throw new Error('Cache failed!');
							} else {
								resolve(time_serie);
							}
						});
					});
				}).on("error", function (err) {
					console.log("Error: " + err.message);
				});
			} else {
				resolve(data);
				console.log('en cache');
			}
		});
	});
};

Api.getDailyEth = function () {
	return new Promise(function (resolve, reject) {
		//simulated error
		if (Math.random(0, 1) < 0.1) {
			throw new Error('How unfortunate! The API Request Failed');
			reject('False api error');
		}

		//Cache
		RedisClient.get('daily_eth', function (err, data) {
			if (err || data === null) {
				_https2.default.get(ETH_DIGITAL_CURRENCY_INTRADAY_URL, function (resp) {
					var data = '';
					resp.on('data', function (chunk) {
						data += chunk;
					});

					resp.on('end', function () {
						var result = JSON.parse(data);
						var by_hours = [];

						_lodash2.default.forEach(result["Time Series (Digital Currency Intraday)"], function (value, key) {
							if (_lodash2.default.endsWith(key, "00:00")) {
								// _.assign(by_hours,{ time: key, value: value })
								by_hours.push({ time: key, value: value });
							}
						});

						var time_serie = _lodash2.default.reverse(_lodash2.default.slice(by_hours, -24));

						//Cache storing
						RedisClient.set('daily_eth', JSON.stringify(time_serie), 'EX', 60 * 60, function (err) {
							if (err) {
								throw new Error('Cache failed!');
							} else {
								resolve(time_serie);
							}
						});
					});
				}).on("error", function (err) {
					console.log("Error: " + err.message);
				});
			} else {
				resolve(data);
				console.log('en cache');
			}
		});
	});
};

Api.getMonthlyEth = function () {
	return new Promise(function (resolve, reject) {
		//simulated error
		if (Math.random(0, 1) < 0.1) {
			throw new Error('How unfortunate! The API Request Failed');
			reject('False api error');
		}

		//Cache
		RedisClient.get('monthly_eth', function (err, data) {
			if (err || data === null) {
				_https2.default.get(ETH_DIGITAL_CURRENCY_MONTHLY_URL, function (resp) {
					var data = '';
					resp.on('data', function (chunk) {
						data += chunk;
					});

					resp.on('end', function () {
						var result = JSON.parse(data);
						var by_months = [];

						_lodash2.default.forEach(result["Time Series (Digital Currency Monthly)"], function (value, key) {
							by_months.push({ time: key, value: value });
						});

						var time_serie = _lodash2.default.reverse(_lodash2.default.slice(by_months, -12)); //last 12 months

						//Cache storing
						RedisClient.set('monthly_eth', JSON.stringify(time_serie), 'EX', 60 * 60 * 24, function (err) {
							if (err) {
								throw new Error('Cache failed!');
							} else {
								resolve(time_serie);
							}
						});
					});
				}).on("error", function (err) {
					console.log("Error: " + err.message);
				});
			} else {
				resolve(data);
				console.log('en cache');
			}
		});
	});
};

exports.default = Api;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("https");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("redis");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("promise-retry");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ })
/******/ ]);