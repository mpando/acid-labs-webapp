const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

const commonModule = {
  rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
            presets: ['env','es2015','react'],
            cacheDirectory: true
        }
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000
          }
        }]
      },
      {
        test: /\.(eot|svg|ttf|woff2?|otf)$/,
        use: 'file-loader'
      }
    ]
};

module.exports = [
    {
        // client
        entry: path.join(__dirname, 'src', 'frontend', 'components', 'App.js'),
        output: {
          path: path.join(__dirname, 'public'),
          filename: 'bundle.js'
        },
        module: commonModule,
        plugins: [
          new HtmlWebpackPlugin({
              template: path.join(__dirname, 'public', 'index.html'),
              favicon: path.join(__dirname, 'public', 'favicon.ico')
            })
          ]
        },
        {
        // server
        entry: path.join(__dirname, 'src', 'backend','modules','server.js'),
        output: {
            path: __dirname,
            filename: 'server.build.js',
        },
        module: commonModule,
        target: 'node',
        externals: [nodeExternals()],
    },
];