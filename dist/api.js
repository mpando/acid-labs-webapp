'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _https = require('https');

var _https2 = _interopRequireDefault(_https);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _redis = require('redis');

var _redis2 = _interopRequireDefault(_redis);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BTC_DIGITAL_CURRENCY_INTRADAY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=BTC&market=CLP&apikey=1B94810TS846HL0M';
var BTC_DIGITAL_CURRENCY_MONTHLY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=BTC&market=CLP&apikey=1B94810TS846HL0M';
var ETH_DIGITAL_CURRENCY_INTRADAY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=ETH&market=CLP&apikey=1B94810TS846HL0M';
var ETH_DIGITAL_CURRENCY_MONTHLY_URL = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=ETH&market=CLP&apikey=1B94810TS846HL0M';

var Api = {};
var RedisClient = _redis2.default.createClient();

Api.getDailyBtc = function () {
	return new Promise(function (resolve, reject) {
		//simulated error
		if (Math.random(0, 1) < 0.1) {
			throw new Error('How unfortunate! The API Request Failed');
			reject('False api error');
		}

		//Cache
		RedisClient.get('daily_btc', function (err, data) {
			if (err || data === null) {
				_https2.default.get(BTC_DIGITAL_CURRENCY_INTRADAY_URL, function (resp) {
					var data = '';
					resp.on('data', function (chunk) {
						data += chunk;
					});

					resp.on('end', function () {
						var result = JSON.parse(data);
						var by_hours = [];

						_lodash2.default.forEach(result["Time Series (Digital Currency Intraday)"], function (value, key) {
							if (_lodash2.default.endsWith(key, "00:00")) {
								// _.assign(by_hours,{ time: key, value: value })
								by_hours.push({ time: key, value: value });
							}
						});

						var time_serie = _lodash2.default.reverse(_lodash2.default.slice(by_hours, -24));

						//Cache storing
						RedisClient.set('daily_btc', JSON.stringify(time_serie), 'EX', 60 * 60, function (err) {
							if (err) {
								throw new Error('Cache failed!');
							} else {
								resolve(time_serie);
							}
						});
					});
				}).on("error", function (err) {
					console.log("Error: " + err.message);
				});
			} else {
				resolve(data);
				console.log('en cache');
			}
		});
	});
};

Api.getMonthlyBtc = function () {
	return new Promise(function (resolve, reject) {
		//simulated error
		if (Math.random(0, 1) < 0.1) {
			throw new Error('How unfortunate! The API Request Failed');
			reject('False api error');
		}

		//Cache
		RedisClient.get('monthly_btc', function (err, data) {
			if (err || data === null) {
				_https2.default.get(BTC_DIGITAL_CURRENCY_MONTHLY_URL, function (resp) {
					var data = '';
					resp.on('data', function (chunk) {
						data += chunk;
					});

					resp.on('end', function () {
						var result = JSON.parse(data);
						var by_months = [];

						_lodash2.default.forEach(result["Time Series (Digital Currency Monthly)"], function (value, key) {
							by_months.push({ time: key, value: value });
						});

						var time_serie = _lodash2.default.reverse(_lodash2.default.slice(by_months, -12)); //last 12 months

						//Cache storing
						RedisClient.set('monthly_btc', JSON.stringify(time_serie), 'EX', 60 * 60 * 24, function (err) {
							if (err) {
								throw new Error('Cache failed!');
							} else {
								resolve(time_serie);
							}
						});
					});
				}).on("error", function (err) {
					console.log("Error: " + err.message);
				});
			} else {
				resolve(data);
				console.log('en cache');
			}
		});
	});
};

Api.getDailyEth = function () {
	return new Promise(function (resolve, reject) {
		//simulated error
		if (Math.random(0, 1) < 0.1) {
			throw new Error('How unfortunate! The API Request Failed');
			reject('False api error');
		}

		//Cache
		RedisClient.get('daily_eth', function (err, data) {
			if (err || data === null) {
				_https2.default.get(ETH_DIGITAL_CURRENCY_INTRADAY_URL, function (resp) {
					var data = '';
					resp.on('data', function (chunk) {
						data += chunk;
					});

					resp.on('end', function () {
						var result = JSON.parse(data);
						var by_hours = [];

						_lodash2.default.forEach(result["Time Series (Digital Currency Intraday)"], function (value, key) {
							if (_lodash2.default.endsWith(key, "00:00")) {
								// _.assign(by_hours,{ time: key, value: value })
								by_hours.push({ time: key, value: value });
							}
						});

						var time_serie = _lodash2.default.reverse(_lodash2.default.slice(by_hours, -24));

						//Cache storing
						RedisClient.set('daily_eth', JSON.stringify(time_serie), 'EX', 60 * 60, function (err) {
							if (err) {
								throw new Error('Cache failed!');
							} else {
								resolve(time_serie);
							}
						});
					});
				}).on("error", function (err) {
					console.log("Error: " + err.message);
				});
			} else {
				resolve(data);
				console.log('en cache');
			}
		});
	});
};

Api.getMonthlyEth = function () {
	return new Promise(function (resolve, reject) {
		//simulated error
		if (Math.random(0, 1) < 0.1) {
			throw new Error('How unfortunate! The API Request Failed');
			reject('False api error');
		}

		//Cache
		RedisClient.get('monthly_eth', function (err, data) {
			if (err || data === null) {
				_https2.default.get(ETH_DIGITAL_CURRENCY_MONTHLY_URL, function (resp) {
					var data = '';
					resp.on('data', function (chunk) {
						data += chunk;
					});

					resp.on('end', function () {
						var result = JSON.parse(data);
						var by_months = [];

						_lodash2.default.forEach(result["Time Series (Digital Currency Monthly)"], function (value, key) {
							by_months.push({ time: key, value: value });
						});

						var time_serie = _lodash2.default.reverse(_lodash2.default.slice(by_months, -12)); //last 12 months

						//Cache storing
						RedisClient.set('monthly_eth', JSON.stringify(time_serie), 'EX', 60 * 60 * 24, function (err) {
							if (err) {
								throw new Error('Cache failed!');
							} else {
								resolve(time_serie);
							}
						});
					});
				}).on("error", function (err) {
					console.log("Error: " + err.message);
				});
			} else {
				resolve(data);
				console.log('en cache');
			}
		});
	});
};

exports.default = Api;