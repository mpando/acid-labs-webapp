'use strict';

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _api = require('./api.js');

var _api2 = _interopRequireDefault(_api);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _promiseRetry = require('promise-retry');

var _promiseRetry2 = _interopRequireDefault(_promiseRetry);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_http2.default.createServer(function (request, response) {
	var headers = {};

	// set header to handle the CORS
	headers['Access-Control-Allow-Origin'] = '*';
	headers['Access-Control-Allow-Headers'] = 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With';
	headers['Access-Contrl-Allow-Methods'] = 'PUT, POST, GET, DELETE, OPTIONS';
	headers["Access-Control-Max-Age"] = '86400';
	headers["Content-Type"] = 'application/json';
	response.writeHead(200, headers);

	console.log(_url2.default.parse(request.url).pathname);

	switch (_url2.default.parse(request.url).pathname) {
		case '/btc_daily.json':
			(0, _promiseRetry2.default)(function (retry, number) {
				console.log('intento numero', number);

				return _api2.default.getDailyBtc().catch(retry);
			}).then(function (result) {
				if (_lodash2.default.isString(result)) result = JSON.parse(result);

				var clp_data = [];
				// let usd_data = [];

				_lodash2.default.forEach(result, function (value, key) {
					clp_data.push({ name: (0, _moment2.default)(value.time).format('h:mm').toString(), price: _lodash2.default.parseInt(value.value["1a. price (CLP)"]) });
					// usd_data.push({name: moment(value.time).format('h:mm').toString(), price: _.parseInt(value.value["1b. price (USD)"]) })
				});

				if (_lodash2.default.isObject(clp_data)) clp_data = JSON.stringify(clp_data);

				response.write(clp_data);
				response.end();
			}, function (err) {
				//..
			});

			break;

		case '/btc_monthly.json':
			(0, _promiseRetry2.default)(function (retry, number) {
				console.log('intento numero', number);

				return _api2.default.getMonthlyBtc().catch(retry);
			}).then(function (result) {
				if (_lodash2.default.isString(result)) result = JSON.parse(result);

				var months = [];
				// let usd_data = [];

				_lodash2.default.forEach(result, function (value, key) {
					months.push({ name: (0, _moment2.default)(value.time).format('MMMM').toString(), price: _lodash2.default.parseInt(value.value["4a. close (CLP)"]) });
				});

				if (_lodash2.default.isObject(months)) months = JSON.stringify(months);
				response.write(months);
				response.end();
			}, function (err) {
				//..
			});
			break;

		case '/eth_daily.json':
			(0, _promiseRetry2.default)(function (retry, number) {
				console.log('intento numero', number);

				return _api2.default.getDailyEth().catch(retry);
			}).then(function (result) {
				if (_lodash2.default.isString(result)) result = JSON.parse(result);

				var clp_data = [];
				// let usd_data = [];

				_lodash2.default.forEach(result, function (value, key) {
					clp_data.push({ name: (0, _moment2.default)(value.time).format('h:mm').toString(), price: _lodash2.default.parseInt(value.value["1a. price (CLP)"]) });
					// usd_data.push({name: moment(value.time).format('h:mm').toString(), price: _.parseInt(value.value["1b. price (USD)"]) })
				});

				if (_lodash2.default.isObject(clp_data)) clp_data = JSON.stringify(clp_data);

				response.write(clp_data);
				response.end();
			}, function (err) {
				//..
			});
			break;

		case '/eth_monthly.json':
			(0, _promiseRetry2.default)(function (retry, number) {
				console.log('intento numero', number);

				return _api2.default.getMonthlyEth().catch(retry);
			}).then(function (result) {
				if (_lodash2.default.isString(result)) result = JSON.parse(result);

				var months = [];
				// let usd_data = [];

				_lodash2.default.forEach(result, function (value, key) {
					months.push({ name: (0, _moment2.default)(value.time).format('MMMM').toString(), price: _lodash2.default.parseInt(value.value["4a. close (CLP)"]) });
				});

				if (_lodash2.default.isObject(months)) months = JSON.stringify(months);
				response.write(months);
				response.end();
			}, function (err) {
				//..
			});
			break;
		default:
			response.write('No valid path');
			response.end();
	}
}).listen(7000);