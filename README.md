# Acid Labs Test
> Aplicación web, requiere NodeJS +6.X y un terminal de comandos.

``` bash
# Desarrollo
npm install
npm install -g babel-cli 
npm run start
```
# Producción
npm install -g pm2
npm run serve-api
npm run serve
```